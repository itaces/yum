$(document).ready(function(){
	$(".main").onepage_scroll({
		sectionContainer: "section",
		responsiveFallback: 620,
		loop: false
	});
	
	var previousStepItem = null,
		currentItemCssClass = 'steps__item--current';
		
	var originalMoveTo = $.fn.moveTo
	$.fn.moveTo = function(section){
		var args = Array.prototype.slice.call(arguments)
		originalMoveTo.apply(this, args)
		
		if (previousStepItem) {
			previousStepItem.removeClass(currentItemCssClass)
		}
		previousStepItem = $('.js-page-0' + section).addClass(currentItemCssClass)
	}
	$(this).moveTo(location.hash.slice(1))
    
	$('.js-success').click(function(){
		$(this).moveTo(2)
	});
	$('.js-apply').click(function(){
		$(this).moveTo(3)
	});
	$('.js-cost').click(function(){
		$(this).moveTo(4)
	});
	$('.js-login').click(function(){
		$(this).moveTo(5)
	});
	
	for(var i = 1; i <= 10; i++) {
		(function(pageNumber){
			$('.js-page-0' + pageNumber).click(function(){
				$(this).moveTo(pageNumber)
			});
		})(i)
	}

	// информацию о плагине можно посмотреть здесь http://dimox.name/jquery-form-styler/
	$('.js-form-style').styler({
		fileBrowse:'Выбрать файл',
		filePlaceholder:'',
	});



	$('.js-link-popup-one').click(function(ev){
		ev.preventDefault();
		$('#popup_one').addClass('active');
	});
	$('.js-link-popup-two').click(function(ev){
		ev.preventDefault();
		$('#popup_two').addClass('active');
	});
	$('.js-link-popup-three').click(function(ev){
		ev.preventDefault();
		$('#popup_three').addClass('active');
	});
	$('.popup-box .close').click(function(ev){
		ev.preventDefault();
		$('.popup-box').removeClass('active');
	});


	$('.top-nav-hide-menu').click(function(event) {
		if($('.top-nav').attr('class').indexOf('active') < 0 ){
			$('.top-nav').addClass('active');
		}
		else{
			$('.top-nav').removeClass('active');
		}
	});


});

$(window).load(function() {
	$('.flexslider').flexslider({
		animation: "slide"
	});
});
