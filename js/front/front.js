//
// throttle-debounce https://github.com/cowboy/jquery-throttle-debounce
// --------------------------------------------------------------------------------
(function(b,c){var $=b.jQuery||b.Cowboy||(b.Cowboy={}),a;$.throttle=a=function(e,f,j,i){var h,d=0;if(typeof f!=="boolean"){i=j;j=f;f=c}function g(){var o=this,m=+new Date()-d,n=arguments;function l(){d=+new Date();j.apply(o,n)}function k(){h=c}if(i&&!h){l()}h&&clearTimeout(h);if(i===c&&m>e){l()}else{if(f!==true){h=setTimeout(i?k:l,i===c?e-m:e)}}}if($.guid){g.guid=j.guid=j.guid||$.guid++}return g};$.debounce=function(d,e,f){return f===c?a(d,e,false):a(d,f,e!==false)}})(this);
// --------------------------------------------------------------------------------

//
// IE Scan
// --------------------------------------------------------------------------------
var isIE11 = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
var isIE8_10 = document.all && document.querySelector;
var isIE8min = '\v'=='v';
// --------------------------------------------------------------------------------

$(document).ready(function () {

    if (isIE11) {
        isIE11 = true;
        $('body').addClass("ie11");
    }
    if (isIE8_10) {
        isIE8_10 = true;
        $('body').addClass('ie8_10');
    }
    if (isIE8min) {
        isIE8min = true;
        $('body').addClass('ie8min');

        $("input, textarea").each(function(index){
            inputPlaceholder = $(this).attr('placeholder');
            if( inputPlaceholder ) {
                $(this).val(inputPlaceholder);
            }
        });
        $('div.column-forum .up ul li').each(function(index) {
            $(this).addClass("up-item-" + ++index);
        });
        $('.table-box.table-box--column-supplier .table-row').each(function(index) {
            $(this).children(".table-cell").each(function(index) {
                $(this).addClass('table-cell-item-' + ++index);
            });
        });
        $('div.column-forum .up ul li:first-child').addClass("up-item-first");
        $('div.column-forum .up ul li:last-child').addClass("up-item-last");
    }



    if ( $('div').hasClass('main') )
    {
        if(( isIE8min !== true ) && ( isIE8_10 !== true ))
        {
            if( $(".main").attr('class').indexOf('franchising') >= 0 )
            {
                $(".main").onepage_scroll({
                    sectionContainer: "section",
                    responsiveFallback: 1101,
                    loop: false,
                    updateURL: true
                });
            }
            else if( $(".main").attr('class').indexOf('project-page') >= 0 )
            {
                $('.hide-nav').addClass('hide-nav-project');

                $(".main").onepage_scroll({
                    sectionContainer: "section",
                    responsiveFallback: 1101,
                    loop: false,
                    beforeMove: function(index) {
                        $('.steps-wrapper .steps li').removeClass('active');
                        $('.js-page-' + index).addClass('active');
                    }
                });

            }
        }
        // Adds a class to the active page in the top navigation (steps of opening a new KFC)
        var previousStepItem = null,
            currentItemCssClass = 'active';

        var originalMoveTo = $.fn.moveTo;
        $.fn.moveTo = function(section){
            var args = Array.prototype.slice.call(arguments);
            originalMoveTo.apply(this, args);

            if (previousStepItem) {
                previousStepItem.removeClass(currentItemCssClass);
            }
            previousStepItem = $('.js-page-' + section).addClass(currentItemCssClass);
        }


        for(var i = 1; i <= 10; i++) {
            (function(pageNumber){
                $('.js-page-' + pageNumber).click(function(){
                    if($(document).width() > 1100 ){
                        $(this).moveTo(pageNumber);
                    }
                    else{
                        $('.steps-wrapper .steps li').removeClass(currentItemCssClass);
                        $('.js-page-' + pageNumber).addClass(currentItemCssClass);
                        $(".wrapper").scrollTop($("#project-page-" + pageNumber ).position().top);
                    }
                });
            })(i)
        }


        $('.js-move-to').click(function(event) {

            // #TODO: To scroll to $ ('html, body') rather than $ ('. Wrapper')
            // #TODO: use "offset" instead of "position"
            // #TODO: class="js-move-to" data-move-to='2' data-move-object='page-2'  you need to append the elements to move to the page

            movePath = $(this).data('moveTo');
            moveObject = $(this).data('moveObject');
            moveObjectScroll = $("#" + moveObject).position().top;

            if ($(document).width() > 1100 ) {
                $(this).moveTo(movePath);
                location.hash = movePath;
            }
            else{
                $(".wrapper").scrollTop(moveObjectScroll);
            }
            return false;
        });
    }

    if ($('.gallery').length) {
        $('.wrapper').addClass('wrapper_gallery');
    }

    if ($('.directors').length) {
        $('.wrapper').addClass('wrapper_directors');
    }




    $('.button-open-menu').click(function(event) {
        $('.header').toggleClass('header_transform');
    });

    // information about the plugin can be found here >> http://dimox.name/jquery-form-styler/
    $('.js-form-style').styler({
        fileBrowse:'Выбрать файл',
        filePlaceholder:'',
    });

    // TODO: Можно сделать для всех input:file, но нужно менять разметку
    if ($('.js-form-style--resume').length) {
        $('.js-form-style--resume').each(function (i) {
            var resumeInputPlaceholder = $(this).attr('placeholder');

            if (!resumeInputPlaceholder) {
                resumeInputPlaceholder = 'Приложить резюме';
            }

            $(this).styler({
                fileBrowse: resumeInputPlaceholder,
                filePlaceholder:''
            });
        })
    }



    // #TODO: rewrite the script popup
    $('.js-link-popup-one').click(function(ev){
        ev.preventDefault();
        $('#popup_one').addClass('active');
    });
    $('.js-link-popup-two').click(function(ev){
        ev.preventDefault();
        $('#popup_two').addClass('active');
    });
    $('.js-link-popup-forum-one').click(function(ev){
        ev.preventDefault();
        $('#popup_forum-one').addClass('active');
    });
    $('.popup-box .close ').click(function(ev){
        ev.preventDefault();
        $('.popup-box , #fade').removeClass('active');
    });

    // opening menu by clicking on the phone versions
    $('.top-nav-hide-menu').click(function(event) {
        if($('.header-top-nav').attr('class').indexOf('active') < 0 ){
            $('.header-top-nav').addClass('active');
        }
        else{
            $('.header-top-nav').removeClass('active');
        }
    });

    $('.customer-rating-edit .customer-rating-value').hover(function () {
        $(this).siblings().removeClass('hover');
        $(this).prevAll().andSelf().addClass('hover');
    }, function () {
        var $parent = $(this).parent();
        var prevVal = $parent.data('original_rating');
        $parent.find('.hover').removeClass('hover');
        $parent.children().slice(0, prevVal).addClass('hover');
    });

    //
    // SUPER КАСТЫЛЬ !!!
    //
    $('.sv_tabs__nav li a').on('click', function () {
        $(this).parent().parent().find('li').removeClass('active');
        $(this).parent().addClass('active');
        var hash = $(this).attr('href');
        $(this).parent().parent().next().find('div').removeClass('active');
        $(this).parent().parent().next().find('div[data-id="'+hash +'"]').addClass('active');
        return false;
    });

});

// --------------------------------------------------------------------------------
// Flex slider
// --------------------------------------------------------------------------------
// slider function
// --------------------------------------------------------------------------------
function FlexVacancySlider(option) {
    var self = this;
    var config = option || {};
    config.wrapper = config.wrapper || '.vacancy_slider';
    config.main = config.main || '.vacancy_slider';

    config.nodeWrapper = $(config.wrapper);
    config.nodeMain = $(config.main);

    config.itemWidth = 0;
    config.itemMinWidth = 170;

    this.setItemWidth = function () {
        config.wrapperWidth = config.nodeWrapper.width();

        if (config.wrapperWidth < 340) {
            config.itemWidth = config.wrapperWidth;
        } else if (config.wrapperWidth < 510) {
            config.itemWidth = config.wrapperWidth / 2;
        } else if (config.wrapperWidth < 680) {
            config.itemWidth = config.wrapperWidth / 3;
        } else if (config.wrapperWidth < 850) {
            config.itemWidth = config.wrapperWidth / 4;
        } else {
            config.itemWidth = config.wrapperWidth / 5;
        }

        if (config.itemWidth < config.itemMinWidth) {
            config.itemWidth = config.itemMinWidth;
        }
    }

    this.init = function () {
        self.setItemWidth();

        config.nodeMain.flexslider({
            animation: 'slide',
            video: true,
            animationLoop: false,
            itemWidth: config.itemWidth,
            itemMargin: 0,
            slideshow: false
        });

        config.slider = config.nodeMain.data('flexslider');
    }

    this.reinit = function () {
        self.setItemWidth();
        config.slider.setOpts({ itemWidth: config.itemWidth });
        // config.slider.resize();
    }

    return this;
}
// --------------------------------------------------------------------------------
// slider action
// --------------------------------------------------------------------------------
$(window).on('load', function () {

    if ($('.vacancy_slider').length) {
        var flexVSlider = new FlexVacancySlider();
        flexVSlider.init();

        $(window).on('resize', $.debounce( 300, function () {
            flexVSlider.reinit();
        }));
    }

    if ($('.directors__slider').length) {
        $('.directors__slider .flexslider').flexslider({
            animation: "slide",
            video: true,
            animationLoop: false,
            itemWidth: 100,
            itemMargin: 0,
            slideshow: false
        });
    }

    if ($('.flexslider').length) {
        $('.flexslider').flexslider({
            animation: "slide",
            video: true
        });
    }
});
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// Gallery
// --------------------------------------------------------------------------------
$(window).load(function () {
    function iniMasonry() {
        $('.gallery').masonry({
          columnWidth: ".item",
          itemSelector: ".item"
        });
    }

    //TODO: сделать нормальный ресайд и убрать одну копию функции
    iniMasonry();
    iniMasonry();
});
// --------------------------------------------------------------------------------
